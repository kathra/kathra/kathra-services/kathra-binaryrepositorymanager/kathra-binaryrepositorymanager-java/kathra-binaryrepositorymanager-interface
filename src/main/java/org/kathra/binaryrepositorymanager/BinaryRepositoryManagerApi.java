package org.kathra.binaryrepositorymanager;

import org.kathra.utils.security.KeycloakUtils;
import org.kathra.core.model.BinaryRepository;
import org.kathra.binaryrepositorymanager.model.Credential;
import org.kathra.core.model.Membership;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.kathra.KathraAuthRequestHandlerImpl;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.rest.RestBindingMode;
import static org.apache.camel.model.rest.RestParamType.*;

@ContextName("BinaryRepositoryManager")
public class BinaryRepositoryManagerApi extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        KeycloakUtils.init();
        // configure we want to use servlet as the component for the rest DSL
        // and we enable json binding mode
        restConfiguration().component(org.kathra.iface.KathraAuthRequestHandler.HTTP_SERVER)
        // use json binding mode so Camel automatic binds json <--> pojo
        .bindingMode(RestBindingMode.off)
        // and output using pretty print
        .dataFormatProperty("prettyPrint", "true")
        .dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES")
        // setup context path on localhost and port number that netty will use
        .contextPath("/api/v1")
        .port("{{env:HTTP_PORT:8080}}")
        .componentProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .endpointProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .consumerProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))

        // add swagger api-doc out of the box
        .apiContextPath("/swagger.json")
        .apiProperty("api.title", "Kathra Binary Repository Manager")
        .apiProperty("api.version", "1.2.0")
        .apiProperty("api.description", "BinaryRepositoryManager")
        // and enable CORS
        .apiProperty("cors", "true")
        .enableCORS(true).corsAllowCredentials(true)
        .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type," +
                "Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

        rest()

        .post("/binaryRepositories/").type(BinaryRepository.class).outType(BinaryRepository.class)
            .description("Add a binary repository")
                .param()
                    .required(true)
                    .name("binaryRepository")
                    .type(body)
                    .description("Binary repository object to be created")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,BinaryRepository.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=addBinaryRepository(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .post("/binaryRepositories/{binaryRepoId}/membership").type(Membership.class)
            .description("Add a binary repository membership for a user or group")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("binaryRepoId")
                    .type(path)
                    .description("The id of the binary repository to retrieve")
                .endParam()
                .param()
                    .required(true)
                    .name("binaryRepositoryMembership")
                    .type(body)
                    .description("Membership object to add to the binary repository")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,Membership.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=addBinaryRepositoryMembership(${header.binaryRepoId},${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/credentials/{id}").outType(Credential.class)
            .description("Return credentials for a user")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("id")
                    .type(path)
                    .description("The id of the binary repository to retrieve")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:BinaryRepositoryManagerController?method=credentialsIdGet(${header.id})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/binaryRepositories/").outType(BinaryRepository[].class)
            .description("Retrieve a list of existing binary repositories for authenticated user")
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=getBinaryRepositories")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/binaryRepositories/{binaryRepoId}").outType(BinaryRepository.class)
            .description("Retrieve a specific binary repository")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("binaryRepoId")
                    .type(path)
                    .description("The id of the binary repository to retrieve")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=getBinaryRepository(${header.binaryRepoId})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/binaryRepositories/{binaryRepoId}/membership").outType(Membership[].class)
            .description("Retrieve a list of users and groups membership values for the specified binary repository")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("binaryRepoId")
                    .type(path)
                    .description("The id of the binary repository to retrieve")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=getBinaryRepositoryMembership(${header.binaryRepoId})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .put("/binaryRepositories/{binaryRepoId}").type(BinaryRepository.class).outType(BinaryRepository.class)
            .description("Fully update a registered binary repository")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("binaryRepoId")
                    .type(path)
                    .description("The id of the binary repository to replace")
                .endParam()
                .param()
                    .required(true)
                    .name("binaryRepository")
                    .type(body)
                    .description("Binary repository object to use to replace existing resource")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,BinaryRepository.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=updateBinaryRepository(${header.binaryRepoId},${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .patch("/binaryRepositories/{binaryRepoId}").type(BinaryRepository.class).outType(BinaryRepository.class)
            .description("Partially update a registered binary repository")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("binaryRepoId")
                    .type(path)
                    .description("The id of the binary repository to partially update")
                .endParam()
                .param()
                    .required(true)
                    .name("binaryRepository")
                    .type(body)
                    .description("Binary repository object to use to patch existing resource")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,BinaryRepository.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleAuthenticatedRequest")
                .to("bean:BinaryRepositoryManagerController?method=updateBinaryRepositoryAttributes(${header.binaryRepoId},${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest();
    }
}