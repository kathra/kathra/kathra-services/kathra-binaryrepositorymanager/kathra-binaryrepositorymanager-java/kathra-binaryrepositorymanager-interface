package org.kathra.binaryrepositorymanager.service;

import org.kathra.core.model.BinaryRepository;
import org.kathra.binaryrepositorymanager.model.Credential;
import org.kathra.core.model.Membership;
import java.util.List;

public interface BinaryRepositoryManagerService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a binary repository
    * 
    * @param binaryRepository Binary repository object to be created (required)
    * @return BinaryRepository
    */
    BinaryRepository addBinaryRepository(BinaryRepository binaryRepository) throws Exception;

    /**
    * Add a binary repository membership for a user or group
    * 
    * @param binaryRepoId The id of the binary repository to retrieve (required)
    * @param binaryRepositoryMembership Membership object to add to the binary repository (required)
    */
    void addBinaryRepositoryMembership(String binaryRepoId, Membership binaryRepositoryMembership) throws Exception;

    /**
    * Return credentials for a user
    * getCredentials
    * @param id The id of the binary repository to retrieve (required)
    * @return Credential
    */
    Credential credentialsIdGet(String id) throws Exception;

    /**
    * Retrieve a list of existing binary repositories for authenticated user
    * 
    * @return List<BinaryRepository>
    */
    List<BinaryRepository> getBinaryRepositories() throws Exception;

    /**
    * Retrieve a specific binary repository
    * 
    * @param binaryRepoId The id of the binary repository to retrieve (required)
    * @return BinaryRepository
    */
    BinaryRepository getBinaryRepository(String binaryRepoId) throws Exception;

    /**
    * Retrieve a list of users and groups membership values for the specified binary repository
    * 
    * @param binaryRepoId The id of the binary repository to retrieve (required)
    * @return List<Membership>
    */
    List<Membership> getBinaryRepositoryMembership(String binaryRepoId) throws Exception;

    /**
    * Fully update a registered binary repository
    * 
    * @param binaryRepoId The id of the binary repository to replace (required)
    * @param binaryRepository Binary repository object to use to replace existing resource (required)
    * @return BinaryRepository
    */
    BinaryRepository updateBinaryRepository(String binaryRepoId, BinaryRepository binaryRepository) throws Exception;

    /**
    * Partially update a registered binary repository
    * 
    * @param binaryRepoId The id of the binary repository to partially update (required)
    * @param binaryRepository Binary repository object to use to patch existing resource (required)
    * @return BinaryRepository
    */
    BinaryRepository updateBinaryRepositoryAttributes(String binaryRepoId, BinaryRepository binaryRepository) throws Exception;

}
